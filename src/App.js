import { Component } from "react";
import CharacterList from "./components/character-list";
import "./App.css";

class App extends Component {
  state = { characterList: [] , nextURL: "https://rickandmortyapi.com/api/character?page=1"};

  getCharacters(url) {
    const { characterList, nextURL} = this.state;
     fetch( nextURL )
       .then((res) => res.json())
       .then((body) => {
           this.setState({
             characterList: [ ...characterList, ...body.results], 
             nextURL: body.info.next
           })
         }
       );
   }

  componentDidMount() {
    this.getCharacters();
  }

  componentDidUpdate(){
    const { nextURL} = this.state;

    if( !nextURL ) return; 

    this.getCharacters();
  }

  render() {
    const { characterList, nextURL } = this.state;

    return (
      <div className="App">
        <header className="App-header">
          <div style={{width:'100vw'}}>
            {!nextURL && <h1>{characterList.length}</h1>}
          </div>
          <CharacterList list={characterList} />
        </header>
      </div>
    );
  }
}

export default App;
